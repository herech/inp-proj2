Autor: Jan Herec
Popis: Program zapsany v jazyce Brainf*ck vypise login: xherec00

++++++++++         bunku cislo 0 inicializovat na 10

[                  pouzit cyklus pro inicializaci nasledujicich tri bunek na 120/100/50
  >++++++++++++    pricist 12 k bunce cislo 1
  >++++++++++      pricist 10 k bunce cislo 2
  >+++++           pricist 5 k bunce cislo 3
  <<<-             vratit se a dekrementovat hodnotu bunky cislo 0
]

>.                 tisk "x"
>++++.             tisk "h"
---.               tisk "e"
<------.           tisk "r"
>.                 tisk "e"
--.                tisk "c"
>--.               tisk "0"
.                  tisk "0"

null               ukoncit program
