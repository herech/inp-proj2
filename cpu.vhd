-- cpu.vhd: Simple 8-bit CPU (BrainFuck interpreter)
-- Copyright (C) 2014 Brno University of Technology,
--                    Faculty of Information Technology
-- Author(s): Jan Herec, xherec00@stud.fit.vutbr.cz
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

-- ----------------------------------------------------------------------------
--                        Entity declaration
-- ----------------------------------------------------------------------------
entity cpu is
 port (
   CLK   : in std_logic;  -- hodinovy signal
   RESET : in std_logic;  -- asynchronni reset procesoru
   EN    : in std_logic;  -- povoleni cinnosti procesoru
 
   -- synchronni pamet RAM
   DATA_ADDR  : out std_logic_vector(12 downto 0); -- adresa do pameti
   DATA_WDATA : out std_logic_vector(7 downto 0); -- mem[DATA_ADDR] <- DATA_WDATA pokud DATA_EN='1'
   DATA_RDATA : in std_logic_vector(7 downto 0);  -- DATA_RDATA <- ram[DATA_ADDR] pokud DATA_EN='1'
   DATA_RDWR  : out std_logic;                    -- cteni (0) / zapis (1)
   DATA_EN    : out std_logic;                    -- povoleni cinnosti
   
   -- vstupni port
   IN_DATA   : in std_logic_vector(7 downto 0);   -- IN_DATA <- stav klavesnice pokud IN_VLD='1' a IN_REQ='1'
   IN_VLD    : in std_logic;                      -- data platna
   IN_REQ    : out std_logic;                     -- pozadavek na vstup data
   
   -- vystupni port
   OUT_DATA : out  std_logic_vector(7 downto 0);  -- zapisovana data
   OUT_BUSY : in std_logic;                       -- LCD je zaneprazdnen (1), nelze zapisovat
   OUT_WE   : out std_logic                       -- LCD <- OUT_DATA pokud OUT_WE='1' a OUT_BUSY='0'
 );
end cpu;


-- ----------------------------------------------------------------------------
--                      Architecture declaration
-- ----------------------------------------------------------------------------
architecture behavioral of cpu is

-- PC register signals
signal pc_reg : std_logic_vector (11 downto 0);
signal pc_dec : std_logic;
signal pc_inc : std_logic;

-- PTR register signals
signal ptr_reg : std_logic_vector (11 downto 0);
signal ptr_dec : std_logic;
signal ptr_inc : std_logic;

-- CNT register signals
signal cnt_reg : std_logic_vector (11 downto 0);
signal cnt_dec : std_logic;
signal cnt_inc : std_logic;

-- MX1 multiplexor signals
signal mx1_sel : std_logic;

-- MX2 multiplexor signals
signal mx2_sel : std_logic_vector (1 downto 0);

-- data_en_sample - auxiliary signal for sampling DATA_EN signal
signal data_en_sample : std_logic; 

-- FSM Machine signals
type FSMstate is (sInitial, sFetch, sDecode, sIncPtr, sDecPtr, sIncMemCell, sDecMemCell, sWhileStart, sPreJumpAfterWhile, sJumpAfterWhile, sWhileEnd, sPreJumpToWhileCond, sJumpToWhileCond, sPrintMemCell, sLoadToMemCell, sHalt, sComment);
signal pstate : FSMstate;
signal nstate : FSMstate;

begin

-- Program counter PC
pc_cntr: process (RESET, CLK)
begin
  if (RESET='1') then
    pc_reg <= (others=>'0');
  elsif (CLK'event) and (CLK='1') then
    if (pc_dec='1') then
      pc_reg <= pc_reg - 1;
    elsif (pc_inc='1') then
      pc_reg <= pc_reg + 1;
    else 
      pc_reg <= pc_reg;
    end if;
  end if;
end process;

-- Pointer to memory PTR
ptr_cntr: process (RESET, CLK)
begin
  if (RESET='1') then
    ptr_reg <= (others=>'0');
  elsif (CLK'event) and (CLK='1') then
    if (ptr_dec='1') then
      if (ptr_reg = X"000") then
        ptr_reg <= X"FFF";
      else
        ptr_reg <= ptr_reg - 1;
      end if;
    elsif (ptr_inc='1') then
      ptr_reg <= ptr_reg + 1;
    else 
      ptr_reg <= ptr_reg;
    end if;
  end if;
end process;

-- Auxilary counter CNT for nested loops
cnt_cntr: process (RESET, CLK)
begin
  if (RESET='1') then
    cnt_reg <= (others=>'0');
  elsif (CLK'event) and (CLK='1') then
    if (cnt_dec='1') then
      cnt_reg <= cnt_reg - 1;
    elsif (cnt_inc='1') then
      cnt_reg <= cnt_reg + 1;
    else 
      cnt_reg <= cnt_reg;
    end if;
  end if;
end process;

-- Program address or data address data multiplexor MX1
with mx1_sel select
DATA_ADDR <= '0' &  pc_reg (11 downto 0) when '0',
             '1' & ptr_reg (11 downto 0) when others; -- others mainly mean '1'

-- Write data multiplexor MX2
with mx2_sel select
DATA_WDATA <= IN_DATA        when "00",
              DATA_RDATA - 1 when "01",
              DATA_RDATA + 1 when others; -- others mainly mean "10"
           
-- sampling signal DATA_EN
DATA_EN <= data_en_sample; 
           
-- Present State register
pstatereg: process (RESET, CLK)
begin
  if (RESET='1') then
    pstate <= sInitial;
  elsif (CLK'event) and (CLK='1') then
    if (EN = '1') then
      pstate <= nstate;
    end if;
  end if;
end process;

-- Next State logic + output logic
nstate_logic: process(pstate, cnt_reg, IN_VLD, OUT_BUSY, DATA_RDATA)
begin
  
  -- INIT
  OUT_WE <= '0';
  OUT_DATA <= X"00";
  IN_REQ <= '0';
  DATA_RDWR <= '0';
  pc_dec <= '0';
  pc_inc <= '0';
  ptr_dec <= '0';
  ptr_inc <= '0';
  cnt_dec <= '0';
  cnt_inc <= '0';
  mx1_sel <= '0';
  mx2_sel <= "00";
  data_en_sample <= '0';
  
  case pstate is
  
    -- INITIAL STATE
    when sInitial =>
      nstate <= sFetch;
    
    -- INSTRUCTION FETCH
    when sFetch =>
      nstate <= sDecode;
      mx1_sel <= '0';
      data_en_sample <= '1';
      
    -- INSTRUCTION DECODE
    when sDecode =>
      case DATA_RDATA is
        -- COMMAND: > (increment pointer to memory for data)
        when X"3E" =>
          nstate <= sIncPtr;
          
        -- COMMAND: < (decrement pointer to memory for data)
        when X"3C" =>
          nstate <= sDecPtr;
        
        -- COMMAND: + (increment value of actual memory cell)
        when X"2B" =>
          mx1_sel <= '1';
          data_en_sample <= '1';
          nstate <= sIncMemCell;
        
        -- COMMAND: - (decrement value of actual memory cell)
        when X"2D" =>
          mx1_sel <= '1';
          data_en_sample <= '1';
          nstate <= sDecMemCell;
        
        -- COMMAND: [ (if value of actual memory cell is not null, continue by the next command, else jump after corresponding command ])
        when X"5B" =>
          mx1_sel <= '1';
          data_en_sample <= '1';
          nstate <= sWhileStart;
          
        -- COMMAND: ] (if value of actual memory cell is not null, jump to corresponding command [, else continue by the next command)
        when X"5D" =>
          mx1_sel <= '1';
          data_en_sample <= '1';
          nstate <= sWhileEnd;
        
        -- COMMAND: . (print value of actual memory cell)        
        when X"2E" =>
          mx1_sel <= '1';
          data_en_sample <= '1';
          nstate <= sPrintMemCell;
          
        -- COMMAND: , (load value from keyboard to actual memory cell)   
        when X"2C" =>
          IN_REQ <= '1';
          nstate <= sLoadToMemCell;
          
        -- COMMAND: null (stop executing program)   
        when X"00" =>
          nstate <= sHalt;
         
        -- PROBABLY COMMENT            
        when others =>
          nstate <= sComment;
      end case;
    
    -- COMMAND: > (increment pointer to memory for data)
    when sIncPtr =>
      nstate <= sFetch;
      pc_inc <= '1';
      ptr_inc <= '1';
      
    -- COMMAND: < (decrement pointer to memory for data)
    when sDecPtr =>
      nstate <= sFetch;
      pc_inc <= '1';
      ptr_dec <= '1';
      
    -- COMMAND: + (increment value of actual memory cell)
    when sIncMemCell =>
      nstate <= sFetch;
      mx1_sel <= '1';
      mx2_sel <= "10";
      DATA_RDWR <= '1';
      data_en_sample <= '1';
      pc_inc <= '1';
      
    -- COMMAND: - (decrement value of actual memory cell)
    when sDecMemCell =>
      nstate <= sFetch;
      mx1_sel <= '1';
      mx2_sel <= "01";
      DATA_RDWR <= '1';
      data_en_sample <= '1';
      pc_inc <= '1';
    
    -- COMMAND: . (print value of actual memory cell) 
    when sPrintMemCell =>
      nstate <= sFetch;
      if (OUT_BUSY = '0') then
        nstate <= sFetch;
        OUT_WE <= '1';
        OUT_DATA <= DATA_RDATA;
        pc_inc <= '1';
      else
        mx1_sel <= '1';
        data_en_sample <= '1';
        nstate <= sPrintMemCell;
      end if;
    
    -- COMMAND: , (load value from keyboard to actual memory cell)  
    when sLoadToMemCell =>
      nstate <= sFetch;
      if (IN_VLD = '0') then
        nstate <= sLoadToMemCell;
        IN_REQ <= '1';
      else
        mx1_sel <= '1';
        mx2_sel <= "00";
        DATA_RDWR <= '1';
        data_en_sample <= '1';
        pc_inc <= '1';
        nstate <= sFetch;
      end if;
      
    -- COMMAND: [ (if value of actual memory cell is not null, continue by the next command, else jump after corresponding command ]) - FIRST PART
    when sWhileStart =>
      pc_inc <= '1';
      nstate <= sFetch;
      if (DATA_RDATA = X"00") then
        cnt_inc <= '1';
        nstate <= sPreJumpAfterWhile;
      else
        nstate <= sFetch;
      end if;
    
    -- COMMAND: [ - SECOND PART 
    when sPreJumpAfterWhile =>
      mx1_sel <= '0';
      data_en_sample <= '1';
      nstate <= sJumpAfterWhile;

    -- COMMAND: [ - THIRD PART 
    when sJumpAfterWhile =>
      nstate <= sFetch;
      if (cnt_reg /= X"000") then     
        if (DATA_RDATA = X"5B") then
          cnt_inc <= '1';
        elsif (DATA_RDATA = X"5D") then
          cnt_dec <= '1';
        end if;   
        pc_inc <= '1';
        nstate <= sPreJumpAfterWhile;
      else
        nstate <= sFetch;
      end if;
          
    -- COMMAND: ] (if value of actual memory cell is not null, jump to corresponding command [, else continue by the next command) - FIRST PART
    when sWhileEnd =>
      nstate <= sFetch;
      if (DATA_RDATA = X"00") then
        pc_inc <= '1';
        nstate <= sFetch;
      else
        cnt_inc <= '1';
        pc_dec <= '1';
        nstate <= sPreJumpToWhileCond;
      end if;
      
    -- COMMAND: ] - SECOND PART 
    when sPreJumpToWhileCond =>
      mx1_sel <= '0';
      data_en_sample <= '1';
      nstate <= sJumpToWhileCond;  
      
    -- COMMAND: ] - THIRD PART 
    when sJumpToWhileCond =>
      nstate <= sFetch;
      if (cnt_reg /= X"000") then 
        pc_dec <= '1';
        pc_inc <= '0';
        if (DATA_RDATA = X"5D") then
          cnt_inc <= '1';
        elsif (DATA_RDATA = X"5B") then
          cnt_dec <= '1';
          if (cnt_reg = "000000000001") then
            pc_inc <= '1';
            pc_dec <= '0';
          end if;  
        end if;         
        
        nstate <= sPreJumpToWhileCond;
      else
        nstate <= sFetch;
      end if;
    
    -- PROBABLY COMMENT
    when sComment =>
      nstate <= sFetch;
      pc_inc <= '1';
    
    -- COMMAND: null (stop executing program)
    when sHalt =>
      nstate <= sHalt;
    
    when others => null;
  
  end case;
end process;

end behavioral;
 
